#include "../inc/config.h"

#include <stdio.h>
#include <stdlib.h>

int
main (void)
{
  fprintf (stdout, "Name: %s\n", FOO_CONFIG_NAME);

  fprintf (stdout, "Version: %s\n", FOO_CONFIG_VERSION);

  fprintf (stdout, "Compiler: %s\n", FOO_CONFIG_COMPILER);

  fprintf (stdout, "Build Type: %s\n", FOO_CONFIG_TYPE);

  fprintf (stdout, "Compiler Flags: %s\n", FOO_CONFIG_CFLAGS);

  fprintf (stdout, "Linker Flags: %s\n", FOO_CONFIG_LDFLAGS);

  fprintf (stdout, "Libraries: %s\n", FOO_CONFIG_LIB);

  return EXIT_SUCCESS;
}
