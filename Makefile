#
# Makefile for this C project.
#
# Environment Overrides:
#   CC - Set the C compiler to use.
#   CFLAGS - Additional compiler flags to use.
#   LDFLAGS - Additional linker flags to use.
#   LIB - Additional libraries to link with.
#
# Commands:
#   debug - Builds a debugging build.
#   release - Builds a release build.
#   build - Builds the project binary.
#   config - Generates the configuration header.
#   clean - Clean generated object/binary files.
#   run   - Runs the generated binary file.
#   valgrind - Runs various valgrind checks.
#   format - Formats all header and source files.
#   directories - Creates all required directories.

#
# User Configuration.
#

# The name of the project.
PROJECT := Foo

# The version number for the project.
VERSION := 1.0

#
# Non-User configuration.
#

# The compiler flags to use.
CFLAGS := $(CFLAGS)

# The linker flags to use.
LDFLAGS := $(LDFLAGS)

# The libraries to use.
LIB := $(LIB)

# The include directory.
INC_DIR := inc

# The source directory.
SRC_DIR := src

# The binary directory.
BIN_DIR := bin

# The valgrind output directory.
VAL_DIR := val

# The object code directory.
OBJ_DIR := obj

# The project name in upper case.
PROJECT_UPPER := $(shell echo $(PROJECT) | tr '[:lower:]' '[:upper:]')

# The project name in lower case.
PROJECT_LOWER := $(shell echo $(PROJECT) | tr '[:upper:]' '[:lower:]')

# The indicators if this is a GNU compiler.
GCC_CHECK := $(shell $(CC) --version | grep -o 'GCC')

IS_GCC := $(shell [ "$(GCC_CHECK)" = 'GCC' ] && echo TRUE || echo FALSE)

# The indicators if this is a Clang compiler.
CLANG_CHECK := $(shell $(CC) --version | grep -o 'clang')

IS_CLANG := $(shell [ "$(CLANG_CHECK)" = 'clang' ] && echo TRUE || echo FALSE)

# The list of source files taken from the source directory.
SRC_FILES := $(wildcard $(SRC_DIR)/*.c)

# The list of to be object files taken from the source directory.
OBJ_FILES := $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC_FILES))

# Set extra compiler options and compiler directive based upon the selected compiler.
ifeq ($(IS_GCC), TRUE)
	CFLAGS := $(CFLAGS) -Wall -Wextra -pedantic-errors -std=c89
	COMPILER := GNU
else ifeq ($(IS_CLANG), TRUE)
	CFLAGS := $(CFLAGS) -Weverything -pedantic-errors -std=c89
	COMPILER := Clang
else
	COMPILER := Unknown
endif

# Silence all output.
.SILENT :

# Standard make command.
all : release

# Debug make command that sets extra debug compiler options and the type to debug.
debug : CFLAGS := $(CFLAGS) -g -O0
debug : TYPE := Debug
debug : build

# Release make command that sets the type to release and turns on optimizations.
release : CFLAGS := $(CFLAGS) -O2
release : TYPE := Release
release : build

# Create required directories, generate configuration header, and build project executable.
build : directories config $(BIN_DIR)/$(PROJECT_LOWER).x

$(BIN_DIR)/$(PROJECT_LOWER).x : $(OBJ_FILES)
	$(CC) $(LDFLAGS) -o $@ $^ $(LIB)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

# Config command to generate a configuration header file.
config : directories
	rm -rf '$(INC_DIR)/config.h'
	echo '#ifndef $(PROJECT_UPPER)_CONFIG_H' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_H' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_NAME "$(PROJECT)"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_VERSION "$(VERSION)"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_COMPILER "$(COMPILER)"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_TYPE "$(TYPE)"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_CFLAGS "$(strip $(CFLAGS))"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_LDFLAGS "$(strip $(LDFLAGS))"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#define $(PROJECT_UPPER)_CONFIG_LIB "$(strip $(LIB))"' >>'$(INC_DIR)/config.h'
	echo '' >>'$(INC_DIR)/config.h'
	echo '#endif /* $(PROJECT_UPPER)_CONFIG_H */' >>'$(INC_DIR)/config.h'

# Clean command that cleans out generated object/binary files.
clean : directories
	rm -rf $(OBJ_DIR)/*
	rm -rf $(BIN_DIR)/*
	rm -rf $(VAL_DIR)/*

# Run command that runs the application.
run : directories
	./$(BIN_DIR)/$(PROJECT_LOWER).x

# Valgrind command that runs various valgrind checks.
valgrind : directories
	valgrind --suppressions=.valgrind --leak-check=full "$(BIN_DIR)/$(PROJECT_LOWER).x"
	valgrind --tool=callgrind --callgrind-out-file="$(VAL_DIR)/callgrind.out.%p" "$(BIN_DIR)/$(PROJECT_LOWER).x"
	valgrind --tool=massif --massif-out-file="$(VAL_DIR)/massif.out.%p" "$(BIN_DIR)/$(PROJECT_LOWER).x"
	valgrind --tool=helgrind "$(BIN_DIR)/$(PROJECT_LOWER).x"

# Format command that formats all header and source files.
format : directories
	for i in `find '$(SRC_DIR)' -maxdepth 1 -type f -name '*.c'`; do clang-format -style=gnu -i "$$i"; done
	for i in `find '$(INC_DIR)' -maxdepth 1 -type f -name '*.h'`; do clang-format -style=gnu -i "$$i"; done

# Directories command that creates required directories if they don't exist.
directories :
	mkdir -p $(INC_DIR)
	mkdir -p $(OBJ_DIR)
	mkdir -p $(VAL_DIR)
	mkdir -p $(BIN_DIR)
