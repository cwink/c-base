def Settings( **kwargs ):
  return {
    'flags': [ '-x', 'c', '-Weverything', '-pedantic-errors', '-std=c89' ],
  }
